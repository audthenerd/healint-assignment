# Expenses Chart for Healint

> A Vue.js assignment that involves the creation of an Expenses Chart to track expenses.     
> Dependency used for chart: [Vue-Chartjs](https://www.npmjs.com/package/vue-chartjs)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
